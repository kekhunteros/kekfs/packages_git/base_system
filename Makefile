PREFIX ?= /data/data/com.team420.kekhunter/files/etc/root

all:
	@echo Run \'make install\' to install Base-System.

install:
	@mkdir -p $(PREFIX)/etc
	@cp -p os-release $(PREFIX)/etc/os-release

uninstall:
	@rm -rf $(PREFIX)/etc/os-release
